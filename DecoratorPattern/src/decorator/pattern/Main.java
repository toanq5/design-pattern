package decorator.pattern;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Computer computer = new Computer();
        computer = new Disk(computer);
        computer = new Monitor(computer);
        computer = new CD(computer);
        System.out.println("You're getting a " + computer.description());
    }
}
