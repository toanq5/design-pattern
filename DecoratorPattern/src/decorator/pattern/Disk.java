package decorator.pattern;

public class Disk extends ComponentDecorator {
    Computer computer;

    public Disk(Computer computer) {
        this.computer = computer;
    }

    public String description() {
        return computer.description() + " add a disk";
    }
}
