package decorator.pattern;

public abstract class ComponentDecorator extends Computer {
    public abstract String description();
}
