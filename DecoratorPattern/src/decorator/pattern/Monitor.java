package decorator.pattern;

public class Monitor extends ComponentDecorator {
    public Monitor(Computer computer) {
        this.computer = computer;
    }

    Computer computer;

    @Override
    public String description() {
        return computer.description() + " add a monitor";
    }
}
