package observer.pattern;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Database database = new Database();
        Archive archive = new Archive();
        Boss boss = new Boss();
        Client client = new Client();


        // register Observer

        database.registerObservers(archive);
        database.registerObservers(client);
        database.registerObservers(boss);

        database.editRecord("delete", "record 1");
    }
}
