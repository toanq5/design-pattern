package observer.pattern;

import java.util.Vector;

public class Database implements Subject {
    private Vector<Observer> observers;
    String operator;
    String record;

    public Database() {
        observers = new Vector<>();
    }

    @Override
    public void registerObservers(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObservers(Observer o) {

        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (int index = 0; index < observers.size(); index++) {
            Observer observer = observers.get(index);
            observer.update(operator, record);
        }
    }

    public void editRecord(String operator, String record) {
        this.operator = operator;
        this.record = record;
        notifyObservers();

    }
}
